const { I } = inject()
const apiKeysPageFunction = require('../page/apiKeysPage/index')
Given('I create API keys', () => {
    apiKeysPageFunction.CreateAPIKey()
})