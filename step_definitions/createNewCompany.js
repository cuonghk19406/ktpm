const { I } = inject()
const createNewCompanyFunction = require('../page/createNewCompany/index.js')
const createNewCompanyLocator = require('../page/createNewCompany/locator.js')
const Myfunctions = require('../page/common/functions');
const MyVariable = require('../page/common/variable.js');
const website = 'https://hoangkimcuong.vn'
const companyName = 'K19406_HoangKimCuong'
Given('I create a new company', () => {
    createNewCompanyFunction.createNewCompany(website, companyName, createNewCompanyLocator.optionsRadio)
});